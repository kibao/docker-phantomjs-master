FROM ubuntu:trusty
MAINTAINER kibao <kibao.pl@gmail.com>

RUN apt-get -yqq update && apt-get -fyq install &&     \
    apt-get install -yq g++ flex bison gperf ruby perl \
    libsqlite3-dev libfontconfig1-dev libicu-dev libfreetype6 libssl-dev \
    libpng-dev libjpeg-dev python libx11-dev libxext-dev  \
    git build-essential libqt5webkit5-dev

RUN git clone https://github.com/ariya/phantomjs.git /tmp/phantomjs && \
  cd /tmp/phantomjs && git checkout master && \
  ./build.sh --confirm --silent && mv bin/phantomjs /usr/local/bin && \
  rm -rf /tmp/phantomjs
